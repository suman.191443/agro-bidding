package com.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.demo.Repository.BidRepository;
import com.demo.Repository.ProductRepository;
import com.demo.Repository.RetailerRepository;
import com.demo.model.Product;
import com.demo.model.Retailer;
import com.demo.model.bidmodel;
import com.demo.model.dto.BidRequestDTO;

import jakarta.servlet.http.HttpSession;

@Controller
public class BidController {
	
	@Autowired
	private BidRepository bidRepo;
	
	@Autowired
	private ProductRepository productRepo;
	
	@Autowired
	private RetailerRepository retailerRepo;
	
	@GetMapping("/bidamount")
	public String bidding(Model model)
	{	
		 
		return "redirect:/bidmount";
	}

	@PostMapping("/bidamount")
	public String bidAmount(@ModelAttribute BidRequestDTO bidReqDTO, HttpSession session) {
	    try {
	    	Product product = new Product();
	    	Retailer retailer = new Retailer();
	    	bidmodel bid = new bidmodel();
	    	
	    	
	        
//        retailer = retailerRepo.findById(bidReqDTO.getId()).get();
//       System.out.println(retailerRepo.findById(bidReqDTO.getId()).get());
	        bid.setP_id(bidReqDTO.getP_id());
	        System.out.println(bidReqDTO.getP_id());
	        bid.setR_id(bidReqDTO.getR_id());
	        System.out.println(bidReqDTO.getR_id());
	        
	    	bid.setBidamount(bidReqDTO.getBidamount());
	    	
	    	
	        bidRepo.save(bid);
	        System.out.println("Bid: " + product.getP_id());
	    } catch (Exception e) {
	    
	        e.printStackTrace();
	    }
	    
	    return "redirect:/bidamount";
	}
	
	
}
