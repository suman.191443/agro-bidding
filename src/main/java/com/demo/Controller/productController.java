package com.demo.Controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.demo.Repository.ProductRepository;
import com.demo.Service.ProductService;
import com.demo.model.Product;

import jakarta.servlet.http.HttpSession;

@Controller
public class productController {

	@Autowired
	private ProductRepository producrRepo;

	@Autowired
	private ProductService productservice;

	public productController(ProductService productservice) {
		super();
		this.productservice = productservice;
	}

	@PostMapping("/product")
	public String saveProduct(@RequestParam("file") MultipartFile file, @ModelAttribute Product product, Model model)
			throws IOException {

		if (!file.isEmpty()) {
			FileOutputStream fout = new FileOutputStream(
					"src/main/resources/static/productImages/" + file.getOriginalFilename());
			fout.write(file.getBytes());
			fout.close();

		}
		String filePath = file.getOriginalFilename();
		product.setImages(filePath);

		producrRepo.save(product);

		return "table";
	}

	@GetMapping("/list")
	public String showProduct(Model model, HttpSession session, Product product, Principal principal) {

//		if(session.getAttribute("rlogin")==null) {
//		
//	return "farmerLogin";
//	
//    }

		List<Product> products = producrRepo.findAll();

		for (Product productss : products) {

			model.addAttribute("plist", producrRepo.findAll());

		}

		return "dashboard";
	}

	@GetMapping("/table")
	public String showtable(HttpSession session) {
//		if (session.getAttribute("rlogin")==null) {
//			return"rlogin";
//		}
		return "table";
	}

}
