package com.demo.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.model.Product;
import com.demo.model.bidmodel;

@Repository
public interface BidRepository extends JpaRepository<bidmodel, Integer> {
	
    @Query(value = "SELECT * FROM bid_tbl WHERE p_id = ?1", nativeQuery = true)
    List<bidmodel> findAllByP_id(int p_id);
}
