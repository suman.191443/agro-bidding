package com.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.model.Farmer;


public interface FarmerRepository extends JpaRepository<Farmer, Integer>{

 

}
